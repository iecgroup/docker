# Docker.
## Requirements.

Linux. 

Docker 20.10.2+. 

Docker compose 1.25.0+.

## Install soft.
```
    sudo apt update && apt upgrade
    sudo apt install docker docker.io containerd docker-compose git zip
```

## Get docker.
```
    mkdir /var/iecgroup && cd /var/iecgroup
    git clone git@gitlab.com:iecgroup/docker.git
```

##	Production environment: before the first launch.
```
    cd iecgroup/docker
    mkdir git && cd git && git clone git@gitlab.com:iecgroup/proxy-encryption.git
    
    mkdir secrets
    echo "password" > secrets/.secret
    cp /key secrets/.key
    
    cd ..
    
    sudo mkdir -p data_main/geth/data && sudo chown -R 1000:1000 data_main/geth/data
    sudo mkdir -p data_main/logs/fpm/{fpm,backend} && sudo chown -R 82:82 data_main/logs/fpm && sudo chmod -R 755 data_main/logs/fpm
    
    #for bsc
    mkdir -p /var/iecgroup/data_main/ && chown -R 1000:1000 /var/iecgroup/data_main/
```

##	Production deploy.
```
    cd docker
    sudo docker-compose -f build.yml build
    sudo docker-compose -f build_bsc.yml build
    sudo docker stack deploy -c stack.yml ethereum
```

##	Production update: example.
```
    sudo docker service update --replicas 1 --force --image ethereum_geth:latest ethereum_ethereum_geth
    sudo docker service update --replicas 0 ethereum_ethereum_geth
    sudo docker service update --replicas 1 ethereum_ethereum_geth
```

##	Production compose (test mode).
```
    cd docker
    sudo docker-compose -f stack.yml up
    sudo docker-compose -f stack_bsc.yml up
```
